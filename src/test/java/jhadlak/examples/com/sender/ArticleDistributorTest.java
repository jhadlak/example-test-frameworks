package jhadlak.examples.com.sender;

import jhadlak.examples.com.article.Article;
import jhadlak.examples.com.client.EntertainmentChannel;
import jhadlak.examples.com.client.OtherChannel;
import jhadlak.examples.com.client.SportsChannel;
import jhadlak.examples.com.database.ArticleDataAccess;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static java.util.Arrays.asList;
import static jhadlak.examples.com.article.Type.POLITICS;
import static jhadlak.examples.com.article.Type.SPORT;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ArticleDistributorTest {
    @Mock
    private SportsChannel sport;

    @Mock
    private EntertainmentChannel entertainment;

    @Mock
    private OtherChannel other;

    @Mock
    private ArticleDataAccess dataAccess;

    @InjectMocks
    ArticleDistributor distributor;

    @Test
    public void sportsGoesToSportPoliticsToOther() {


        // given this list of articles is returned from database
        List<Article> list = asList(
                new Article("sport is fun", SPORT),
                new Article("Politics is sad", POLITICS));

        when(dataAccess.getTodaysArticles()).thenReturn(list);

        // when we distribute
        distributor.distributeTodays();

        // then one goes to sport and one goes to finance
        verify(sport).accept(any());
        verify(other).accept(any());
        verify(entertainment, never()).accept(any());
    }
}