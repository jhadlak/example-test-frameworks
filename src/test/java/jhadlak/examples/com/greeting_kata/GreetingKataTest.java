package jhadlak.examples.com.greeting_kata;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GreetingKataTest {

    @Test
    public void greetSaysHello() {
        Assertions.assertThat(GreetingKata.greet("Sonja")).isEqualTo("Hallo, Sonja.");
    }

    @Test
    public void greetHandlesNulls() {
        String name = null;
        Assertions.assertThat(GreetingKata.greet(name)).isEqualTo("Hallo, mein Freund.");
    }

    @Test
    public void greetHandlesEmptyString() {
        String name = "";
        Assertions.assertThat(GreetingKata.greet(name)).isEqualTo("Hallo, mein Freund.");
    }

    @Test
    public void greetShoutsBack() {
        Assertions.assertThat(GreetingKata.greet("ANJA")).isEqualTo("HALLO ANJA!");
    }

    @Test
    public void greetTwoPeople() {
        Assertions.assertThat(GreetingKata.greet(Arrays.asList("Sonja", "Anja"))).isEqualTo("Hallo, Sonja und Anja.");
    }

    @Test
    public void greetMoreThanTwoPeople() {
        Assertions.assertThat(GreetingKata.greet(Arrays.asList("Sonja", "Anja", "Elisa")))
                .isEqualTo("Hallo, Sonja, Anja, und Elisa.");
    }

    @Test
    public void greetOnePersonInList() {
        Assertions.assertThat(GreetingKata.greet(Arrays.asList("Sonja")))
                .isEqualTo("Hallo, Sonja.");
    }

    @Test
    public void greetShoutsBackOnePersonInList() {
        Assertions.assertThat(GreetingKata.greet(Arrays.asList("SONJA")))
                .isEqualTo("HALLO SONJA!");
    }

    @Test
    public void greetHandleNullList() {
        List<String> namesToGreet = null;
        Assertions.assertThat(GreetingKata.greet(namesToGreet))
                .isEqualTo("Hallo, mein Freund.");
    }

    @Test
    public void greetHandleEmptyList() {
        List<String> namesToGreet = new ArrayList<>();
        Assertions.assertThat(GreetingKata.greet(namesToGreet))
                .isEqualTo("Hallo, mein Freund.");
    }

    @Test
    public void greetMultiplePeopleAndShoutBack() {
        Assertions.assertThat(GreetingKata.greet(Arrays.asList("Sonja", "ANJA", "Elisa")))
                .isEqualTo("Hallo, Sonja und Elisa. UND HALLO ANJA!");
    }

    @Test
    public void greetMultiplePeopleAndShoutBack2() {
        Assertions.assertThat(GreetingKata.greet(Arrays.asList("Sonja", "ANJA", "Elisa", "TANJA")))
                .isEqualTo("Hallo, Sonja und Elisa. UND HALLO ANJA UND TANJA!");
    }

    @Test
    public void greetMultipleShoutingPeople() {
        Assertions.assertThat(GreetingKata.greet(Arrays.asList("SONJA", "ANJA", "ELISA", "TANJA")))
                .isEqualTo("HALLO SONJA, ANJA, ELISA, UND TANJA!");
    }

    @Test
    public void greetMultiplePeopleSeparatedByComma() {
        Assertions.assertThat(GreetingKata.greet(Arrays.asList("SONJA, Johanna, Maria", "ANJA", "ELISA", "TANJA")))
                .isEqualTo("Hallo, Johanna und Maria. UND HALLO SONJA, ANJA, ELISA, UND TANJA!");
    }

    @Test
    public void greetEscapeIntentionalCommas() {
        Assertions.assertThat(GreetingKata.greet(Arrays.asList("Bob", "\"Charlie, Dianne\"")))
                .isEqualTo("Hallo, Bob und Charlie, Dianne.");
    }

}