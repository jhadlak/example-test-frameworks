package jhadlak.examples.com.asserts;

import jhadlak.examples.com.geometry.Rectangle;
import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.offset;

public class AssertionAssertJTest {

    @Test
    public void tryAssertTrue() {
        Rectangle rectangle = new Rectangle(10, 10);

        assertThat(rectangle.isSquare()).isTrue();
    }

    @Test
    public void tryAssertTrue2() {
        Rectangle rectangle = new Rectangle(10, 10);

        assertThat(rectangle.isSquare())
                .describedAs("Rectangle with same width and height should be Square")
                .isTrue();
    }

    @Test
    public void tryAssertFalse() {
        Rectangle rectangle = new Rectangle(11, 10);

        assertThat(rectangle.isSquare()).isFalse();
    }

    @Test
    public void tryAssertNull() {
        Object value = null;

        assertThat(value).isNull();
    }

    @Test
    public void tryAssertNotNull() {
        Rectangle rectangle = new Rectangle(1, 1);

        assertThat(rectangle).isNotNull();
    }

    @Test
    public void tryAssertEquals() {
        Rectangle rec1 = new Rectangle(2, 8);

        assertThat(rec1.getArea()).isEqualTo(16);
    }

    @Test
    public void tryAssertEqualsWithStrings() {
        assertThat("Hello\nWORLD\nnow").isEqualTo("Hello\nWORLD\nnow"
        );
    }

    @Test
    public void tryAssertNotEquals() {
        Rectangle rec1 = new Rectangle(2, 8);

        assertThat(rec1.getArea()).isNotEqualTo(17);
    }

    @Test
    public void tryAssertEqualsWithDouble() {
        double aspectRatio = new Rectangle(1,3).getAspectRatio();

        assertThat(aspectRatio).isCloseTo(0.33333333333333, offset(0.001));
    }

    @Ignore
    @Test
    public void tryAssertEqualsWithObject(){
        Rectangle rec1 = new Rectangle(2, 8);
        Rectangle rec2 = new Rectangle(2, 8);

        System.out.println(rec1.compareTo(rec2));

        assertThat(new Rectangle(1, 1)).isEqualTo(new Rectangle(1, 1));
    }

    @Test
    public void tryAssertSame(){
        Rectangle rec1 = new Rectangle(2, 8);
        Rectangle rec2 = new Rectangle(2, 8);

        System.out.println(rec1.compareTo(rec2));

        assertThat(rec1).isSameAs(rec1);
        assertThat(rec1).isNotSameAs(rec2);
    }

    @Test
    public void tryAssertArray(){
        Rectangle rec1 = new Rectangle(2, 8);

        assertThat(rec1.getSides()).containsExactly(2, 8);
    }

    @Test
    public void tryAssertList(){
        Rectangle rec1 = new Rectangle(2, 8);

        assertThat(rec1.getSidesAsList()).containsExactly(2d, 8d);
    }

}
