package jhadlak.examples.com.asserts;

import jhadlak.examples.com.geometry.Rectangle;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.*;

public class AssertionJUnitTest {

    @Test
    public void testAssertTrue() {
        int val1 = 5;
        int val2 = 6;

        assertTrue(val1 < val2);
    }

    @Test
    public void testAssertFalse() {
        int val1 = 5;
        int val2 = 6;

        assertFalse(val1 > val2);
    }

    @Test
    public void testAssertEquals() {
        String str1 = new String("abc");
        String str2 = new String("abc");

        assertEquals(str1, str2);
    }

    @Test
    public void testAssertNotNull() {
        String str1 = new String("abc");

        assertNotNull(str1);
    }

    @Test
    public void testAssertNull() {
        String nullString = null;

        assertNull(nullString);
    }

    @Test
    public void testAssertSameWithStrings() {
        String str4 = "abc";
        String str5 = "abc";

        assertSame(str4, str5);
    }

    @Test
    public void tryAssertSameWithObjects(){
        Rectangle rec1 = new Rectangle(2, 8);
        Rectangle rec2 = new Rectangle(2, 8);

        assertSame(rec1, rec1);
    }

    @Test
    public void tryAssertNotSameWithObjects(){
        Rectangle rec1 = new Rectangle(2, 8);
        Rectangle rec2 = new Rectangle(2, 8);

        assertNotSame(rec1, rec2);
    }

    @Test
    public void testAssertNotSame() {
        String str1 = new String("abc");
        String str3 = null;

        assertNotSame(str1, str3);
    }

    @Test
    public void testAssertArrayEquals(){
        String[] expectedArray = {"one", "two", "three"};
        String[] resultArray = {"one", "two", "three"};

        assertArrayEquals(expectedArray, resultArray);
    }

    @Ignore
    @Test
    public void tryAssertEqualsWithObject(){
        Rectangle rec1 = new Rectangle(2, 8);
        Rectangle rec2 = new Rectangle(2, 8);

        System.out.println(rec1.compareTo(rec2));

        assertEquals(new Rectangle(1, 1), new Rectangle(1, 1));
    }

}
