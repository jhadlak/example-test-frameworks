package jhadlak.examples.com.string_calculator_kata;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class StringCalculatorKataTest {

    @Test
    public void forAnEmptyStringZeroShouldBeReturned() {
        Assertions.assertThat(StringCalculatorKata.add(null)).isEqualTo(0);
    }

    @Test
    public void forASingleNumberTheSingleNumberShouldBeReturned() {
        Assertions.assertThat(StringCalculatorKata.add("5")).isEqualTo(5);
    }

    @Test
    public void theSumOfTwoGivenNumbersShouldBeReturned(){
        Assertions.assertThat(StringCalculatorKata.add("1,2")).isEqualTo(3);
    }

    @Test
    public void theSumOfAnyAmountOfNumbersShouldBeReturned(){
        Assertions.assertThat(StringCalculatorKata.add("1,2,5,6,7")).isEqualTo(21);
    }

    @Test
    public void newLinesShouldBeAcceptedAsDelimiterBetweenAddends() {
        Assertions.assertThat(StringCalculatorKata.add("1\n2,5\n6,7")).isEqualTo(21);
        Assertions.assertThat(StringCalculatorKata.add("1,\n")).isEqualTo(1);
    }

}
