package jhadlak.examples.com.mockito;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MockitoExamplesTest {
    @Mock
    private SomeInterface someInterface;

    @Captor
    private ArgumentCaptor<List<String>> listCaptor;

    @Test
    public void exampleTest() {
        when(someInterface.getMatchingSize(9, 2))
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(3)
                .thenReturn(2)
                .thenReturn(1);

        for (int i = 0; i < 5; i++) {
            System.out.println(someInterface.getMatchingSize(9, 2));
        }
    }

    @Test
    public void verifyMultipleExample() {
        someInterface.getMatchingSize(1, 2);
        someInterface.getMatchingSize(1, 2);

        verify(someInterface, times(2)).getMatchingSize(1, 2);
    }

    @Test
    public void verifyConcurrent() {
        new Thread(() -> {
            for (int i = 0; i < 20; i++) {
                someInterface.getMatchingSize(1, 2);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                }
            }
        }).start();
        verify(someInterface, timeout(2000).times(20)).getMatchingSize(1, 2);
//        verify(someInterface, timeout(2000).atLeast(10)).getMatchingSize(1, 2);
    }

    @Test
    public void verifyArgument() {
        someInterface.receiveList(asList("A", "B", "C"));
        verify(someInterface).receiveList(listCaptor.capture());

        assertEquals(3, listCaptor.getValue().size());
    }

    @Test
    public void verifyArgumentWithoutCaptors() {
        someInterface.receiveList(asList("A", "B", "C"));

        verify(someInterface).receiveList(argThat(list -> list.size() == 3));
        verify(someInterface).receiveList(argThat(list -> list.get(0).equals("A")));
    }

    @Test(expected = IOException.class)
    public void willThrow() throws Exception {
        when(someInterface.isFileValid("myfile")).thenThrow(new IOException("Boom"));

        someInterface.isFileValid("myfile");
    }

    @Test
    public void thenAnswer() {
        when(someInterface.getMatchingSize(anyInt(), anyInt())).thenAnswer(new Answer<Integer>() {

            @Override
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                return (int) invocation.getArguments()[0] + (int) invocation.getArguments()[1];
            }
        });

        assertEquals(5, someInterface.getMatchingSize(2, 3));
        assertEquals(6, someInterface.getMatchingSize(2, 4));
        assertEquals(7, someInterface.getMatchingSize(2, 5));
        assertEquals(8, someInterface.getMatchingSize(2, 6));
    }

    @Test
    public void spying() {
        Set<Integer> set = spy(new LinkedHashSet<>());

        calculatePrimes(set, 100);

        assertTrue(set.contains(31));

        verify(set).add(31);
    }

    private static void calculatePrimes(Set<Integer> primes, int max) {
        for (int i = 2; i <= max; i++) {
            if (!devisibleByAny(i, primes)) {
                primes.add(i);
            }
        }
    }

    private static boolean devisibleByAny(int newNumber, Set<Integer> primes) {
        for (Integer prime : primes) {
            if (newNumber % prime == 0) {
                return true;
            }
        }
        return false;
    }
}
