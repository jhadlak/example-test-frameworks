package jhadlak.examples.com.reader_writer;

import jhadlak.examples.com.ReaderWriter;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ReaderWriterHamcrestTest {

    private File writtenFile;

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @After
    public void ensureFileIsGone() {
        assertThat(writtenFile.delete(), is(true));
    }

    @Test
    public void readsLinesItWrote() throws Exception {
        // when we write the file
        List<String> someLines = asList("One", "Two", "Three");
        writtenFile = ReaderWriter.write(temporaryFolder.getRoot(), "MyFile", someLines);

        // then we can read it back
        List<String> readLines = ReaderWriter.read(writtenFile);

        // and they match
        assertThat(someLines, CoreMatchers.equalTo(readLines));
    }
}
