package jhadlak.examples.com.reader_writer;

import jhadlak.examples.com.ReaderWriter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ReaderWriterWithBeforeAndAfterTest {

    private File tempFolder;
    private File writtenFile;
    private List<String> someLines;

    @Before
    public void setup() throws Exception {
        // with a temporary folder to write to
        tempFolder = File.createTempFile("tmp", "file");
        tempFolder.delete();
        tempFolder.mkdir();
    }

    @After
    public void teardown() throws Exception {
        // and the tidy up is successful
        assertTrue(writtenFile.delete());
//        assertTrue(tempFolder.delete()); // TODO should work acutally but causes error
    }

    @Test
    public void readsLinesItWrote() throws Exception {
        // when we write the file
        someLines = asList("One", "Two", "Three");
        writtenFile = ReaderWriter.write(tempFolder, "MyFile", someLines);

        // then we can read it back
        List<String> readLines = ReaderWriter.read(writtenFile);

        // and they match
        assertEquals(someLines, readLines);
    }
}
