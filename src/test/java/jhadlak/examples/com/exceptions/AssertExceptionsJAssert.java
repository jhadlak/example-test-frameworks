package jhadlak.examples.com.exceptions;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.IOException;

public class AssertExceptionsJAssert {

    @Test
    public void willThrowThrows() throws Exception {
        Assertions.assertThatThrownBy(() -> willThrow())
                .isInstanceOf(IOException.class)
                .hasMessage("Bang");
    }

    private static void willThrow() throws IOException {
        throw new IOException("Bang");
    }
}
