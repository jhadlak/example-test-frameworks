package jhadlak.examples.com.leap_year;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class DateUtilTest {

    @Test
    public void normalLeapYearIsLeap() {
        assert(DateUtil.isLeapYear(1992));
    }

    @Test
    public void normalLeapYearExample2IsLeap() {
        assert(DateUtil.isLeapYear(1996));
    }

    @Test
    public void nonLeapYearIsNotLeap() {
        assertFalse(DateUtil.isLeapYear(1991));
    }

    @Test
    public void centuryYearsAreNotLeap() {
        assertFalse(DateUtil.isLeapYear(1900));
    }

    @Test
    public void centuryYear2000WasLeap() {
        assert(DateUtil.isLeapYear(2000));
    }
}