package jhadlak.examples.com.geometry;

import lombok.Getter;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Getter
public class Rectangle implements Comparable<Rectangle> {

    private static double DEFAULT_WITH = 0;
    private static double DEFAULT_LENGTH = 0;

    private double width;
    private double length;

    public Rectangle() {
        this(DEFAULT_WITH, DEFAULT_LENGTH);
    }

    public Rectangle(double[] sides) {
        this(sides[0], sides[1]);
    }

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public boolean isSquare() {
        return width == length;
    }

    public double getArea() {
        return width * length;
    }

    public double getAspectRatio() {
        return Math.min(width, length) / Math.max(width, length);
    }

    @Override
    public int compareTo(Rectangle o) {
        return Comparator
                .comparing(Rectangle::getArea)
                .compare(this, o);
    }

    public double[] getSides() {
        return new double[]{width, length};
    }

    public List<Double> getSidesAsList() {
        return Arrays.asList(width, length);
    }
}
