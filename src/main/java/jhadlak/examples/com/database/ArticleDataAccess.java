package jhadlak.examples.com.database;

import jhadlak.examples.com.article.Article;

import java.util.List;

public interface ArticleDataAccess {
    /**
     * @return all the articles from today
     */
    List<Article> getTodaysArticles();
}
