package jhadlak.examples.com.article;

import lombok.Getter;

/**
 * An article a client may be interested in
 */
@Getter
public class Article {
    private String content;
    private Type type;

    public Article(String content, Type type) {
        this.content = content;
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}
