package jhadlak.examples.com.leap_year;

public class DateUtil {

    /**
     * is leap year
     * @param year 4 digit year
     * @return true if leap year
     */
    public static boolean isLeapYear(int year) {
        return isDevisibleBy(year, 4) && (!isDevisibleBy(year, 100) || isDevisibleBy(year, 400));
    }

    private static boolean isDevisibleBy(int year, int devisor){
        return (year % devisor) == 0;
    }
}

