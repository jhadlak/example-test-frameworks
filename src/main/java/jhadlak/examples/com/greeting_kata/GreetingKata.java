package jhadlak.examples.com.greeting_kata;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GreetingKata {

    public static final String DEFAULT_NAME = "mein Freund";

    public static final String SAY_HELLO = "Hallo, ";
    public static final String SAY_PHRASE_END = ".";
    public static final String SAY_AND_TWO_PEOPLE = " und ";
    public static final String SAY_AND_SEVERAL_PEOPLE = ", und ";
    public static final String SAY_COMMA = ", ";

    public static final String SHOUT_HELLO = "HALLO ";
    public static final String SHOUT_PHRASE_END = "!";
    public static final String SHOUT_AND_TWO_PEOPLE = " UND ";
    public static final String SHOUT_AND_SEVERAL_PEOPLE = ", UND ";


    public static String greet(String name) {
        return name == null || name.isEmpty() ? greetUnknownFriend() : greet(Arrays.asList(name));
    }

    public static String greet(List<String> namesToGreet) {

        if (namesToGreet == null || namesToGreet.isEmpty()) {
            return greetUnknownFriend();
        }

        List<String> allNames =
                separateByCommaButEscapeQuotes(String.join(",", namesToGreet))
                .stream()
                .filter(name -> !name.isEmpty())
                .map(name -> name.replace("\"", "").trim())
                .collect(Collectors.toList());

        List<String> namesToGreetNormal = allNames.stream()
                .filter(name -> name != name.toUpperCase())
                .collect(Collectors.toList());
        List<String> namesToGreetShouting = allNames.stream()
                .filter(name -> name.equals(name.toUpperCase()))
                .collect(Collectors.toList());

        String delimiterNormalAndShouting = namesToGreetNormal.isEmpty() || namesToGreetShouting.isEmpty()
                ? ""
                : " UND ";

        return getGreetingPhrase(namesToGreetNormal, false)
                + delimiterNormalAndShouting
                + getGreetingPhrase(namesToGreetShouting, true);
    }

    private static String greetUnknownFriend(){
        return SAY_HELLO + DEFAULT_NAME + SAY_PHRASE_END;
    }

    private static String getGreetingPhrase(List<String> namesToGreet, boolean shout){
        String hello = shout ? SHOUT_HELLO : SAY_HELLO;
        String phraseEnd = shout ? SHOUT_PHRASE_END : SAY_PHRASE_END;
        String andForTwo = shout ? SHOUT_AND_TWO_PEOPLE : SAY_AND_TWO_PEOPLE;
        String andForSeveral = shout ? SHOUT_AND_SEVERAL_PEOPLE : SAY_AND_SEVERAL_PEOPLE;

        String greetingPhrase = "";
        if (namesToGreet.size() == 1) {
            greetingPhrase = hello + namesToGreet.get(0) + phraseEnd;
        }
        if (namesToGreet.size() == 2) {
            greetingPhrase = hello + namesToGreet.get(0) + andForTwo + namesToGreet.get(1) + phraseEnd;
        }
        if (namesToGreet.size() >= 3) {
            String firstNames = String.join(SAY_COMMA, namesToGreet.subList(0, namesToGreet.size()-1));
            String lastName = namesToGreet.get(namesToGreet.size()-1);
            greetingPhrase = hello
                    + firstNames
                    + andForSeveral
                    + lastName
                    + phraseEnd;
        }
        return greetingPhrase;
    }

    private static List<String> separateByCommaButEscapeQuotes(String names){
        return Arrays.asList( names.split(",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)"));
    }
}
