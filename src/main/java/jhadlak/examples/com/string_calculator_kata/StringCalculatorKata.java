package jhadlak.examples.com.string_calculator_kata;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public class StringCalculatorKata {

    public static final String ANY_NEWLINES_OR_COMMAS = "[\\n,](?!,\\n)"; // [^,]

    public static int add(String numbers){
        if (StringUtils.isEmpty(numbers)) {return 0;}
        return Arrays.stream(numbers.split(ANY_NEWLINES_OR_COMMAS)).mapToInt(Integer::valueOf).sum();
    }
}
