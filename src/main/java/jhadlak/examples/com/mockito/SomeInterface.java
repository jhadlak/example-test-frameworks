package jhadlak.examples.com.mockito;

import java.io.IOException;
import java.util.List;

public interface SomeInterface {
    Object getMatchingSize(int anyInt, int anyInt1);

    boolean receiveList(List someList);

    boolean isFileValid(String myfile) throws IOException;
}
